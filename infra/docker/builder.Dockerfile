FROM node:17.4.0-alpine

COPY . /app
WORKDIR /app

RUN apk add make

RUN npm install -g pnpm
RUN NPM=pnpm make setup build
