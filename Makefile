.PHONY: all setup \
				print-version changelog \
				check-tool-git-cliff \
				build build-watch run \
# Release Automation
				release-major release-minor release-patch \
				print-image-version check-docker-config docker-registry-login \
				image image-publish image-release \
				builder-image builder-image-publish

PNPM ?= pnpm
NODE ?= node
DOCKER ?= docker
GIT ?= git
GIT_CLIFF ?= git-cliff

VERSION ?= $(shell $(NODE) -e 'console.log(require("./package.json").version);')
CURRENT_SHA ?= $(shell $(GIT) rev-parse --short HEAD)

CHANGELOG_FILE_PATH ?= CHANGELOG

PROJECT_NAME ?= pvcloney

all: build image publish

print-version:
	@echo -e -n "$(VERSION)"

check-tool-git-cliff:
ifeq (,$(shell which $(GIT_CLIFF)))
	$(error "git-cliff is not installed (https://github.com/orhun/git-cliff)")
endif

check-tool-docker:
ifeq (,$(shell which $(DOCKER)))
	$(error "docker (or some other container runtime) is not installed (https://docs.docker.com)")
endif

changelog: check-tool-git-cliff
	$(GIT) cliff --unreleased --tag=$(VERSION) --prepend=$(CHANGELOG_FILE_PATH)

###########
# Project #
###########

setup:
	@$(PNPM) --silent install

build: setup
	@$(PNPM) --silent build

build-watch:
	$(PNPM) --silent "build:watch"

run:
	@$(PNPM) --silent start -- $(ARGS)

######################
# Release Automation #
######################

release-major:
	$(PNPM) version major --no-git-tag-version
	$(MAKE) -s --no-print-directory changelog
	$(GIT) commit -am "release: v`$(NODE) -e 'console.log(require("./package.json").version);'`"
	$(GIT) push

release-minor:
	$(PNPM) version minor --no-git-tag-version
	$(MAKE) -s --no-print-directory changelog
	$(GIT) commit -am "release: v`$(NODE) -e 'console.log(require("./package.json").version);'`"
	$(GIT) push

release-patch:
	$(PNPM) version patch --no-git-tag-version
	$(MAKE) -s --no-print-directory changelog
	$(GIT) commit -am "release: v`$(NODE) -e 'console.log(require("./package.json").version);'`"
	$(GIT) push

##########
# Docker #
##########

DOCKER_JSON_SECRET_PATH ?= ./secrets/docker/.dockerconfigjson

CONTAINER_REGISTRY=registry.gitlab.com
REPO=registry.gitlab.com/mrman/$(PROJECT_NAME)

IMAGE_NAME=cli
IMAGE_FULL_NAME_SHA=${REPO}/${IMAGE_NAME}:${VERSION}-${CURRENT_SHA}
IMAGE_FULL_NAME=${REPO}/${IMAGE_NAME}:${VERSION}

BUILDER_VERSION ?= latest

BUILDER_IMAGE_NAME=builder
BUILDER_IMAGE_FULL_NAME=${REPO}/${BUILDER_IMAGE_NAME}:${BUILDER_VERSION}

print-image-version:
	@echo -e -n "$(VERSION_WITH_SHA)"

SECRET_REGISTRY_USERNAME_PATH ?= secrets/gitlab/deploy-tokens/dev.username.secret
SECRET_REGISTRY_PASSWORD_PATH ?= secrets/gitlab/deploy-tokens/dev.password.secret

check-docker-config:
ifeq (,$(DOCKER_CONFIG))
	$(error "DOCKER_CONFIG not set (it should be set to a local dir for this project)")
endif

docker-registry-login: check-docker-config
	@if [ ! -f $(DOCKER_JSON_SECRET_PATH) ] ; then \
		cat $(SECRET_REGISTRY_PASSWORD_PATH) | $(DOCKER) login $(CONTAINER_REGISTRY) -u $(shell cat $(SECRET_REGISTRY_USERNAME_PATH)) --password-stdin; \
	fi

image: check-tool-docker
	$(DOCKER) build \
	-f infra/docker/Dockerfile \
	-t ${IMAGE_FULL_NAME_SHA} \
	.

image-publish: check-tool-docker
	$(DOCKER) push ${IMAGE_FULL_NAME_SHA}

image-release:
	$(DOCKER) tag $(IMAGE_FULL_NAME_SHA) $(IMAGE_FULL_NAME)
	$(DOCKER) push $(IMAGE_FULL_NAME)

builder-image: check-tool-docker
	$(DOCKER) build \
	-f infra/docker/builder.Dockerfile \
	-t ${BUILDER_IMAGE_FULL_NAME} \
	.
builder-image-publish: check-tool-docker
	$(DOCKER) push ${BUILDER_IMAGE_FULL_NAME}
