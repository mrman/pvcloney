import * as fs from "fs";
import * as path from "path";

import { generate as generateRandomString } from "randomstring";

import { fileURLToPath } from "url";
import process from "process";
import { program, Command } from "commander";
import { PVCCopyArgs, checkPVCCopyArgs } from "./types.js";
import { waitFor } from "async-wait-for-promise";

import * as k8s from "@kubernetes/client-node";

import { dirname }  from "dirname-filename-esm";

const PACKAGE_JSON_PATH = path.resolve(path.join(dirname(import.meta), "../package.json"));
const pkginfo = JSON.parse(fs.readFileSync(PACKAGE_JSON_PATH).toString());
const VERSION = pkginfo.version;

const DEFAULT_TIMEOUT_MS = 1000 * 60 * 3;
const REPLICA_SCALE_TIMEOUT_MS = 1000 * 60 * 5;
const ORIGINAL_PVC_DELETE_TIMEOUT_MS = 1000 * 60 * 10;
const COPY_JOB_TIMEOUT_MS = 1000 * 60 * 30 ;

import { default as getDefaultLogger, Logger } from "pino";

async function connectToK8s(): Promise<k8s.KubeConfig> {
  const kc = new k8s.KubeConfig();
  kc.loadFromDefault();
  return kc;
}

interface BuildArgsArgs {
  // CLI arguments parsed by commander
  parsed: Record<string, string | number | boolean>;

  env: NodeJS.ProcessEnv;
  argv: string[];
  logger?: Logger;
}

/**
 * Build arguments object necessary to perform PVC copy from env and other sources
 *
 * @param {object} args
 * @param {object} args.parsed - CLI args parsed by commander
 * @param {object} args.env - env, usually process.env
 * @param {string[]} args.args - process.argv
 * @param {Logger} [args.logger]
 * @returns {Promise<PVCCopyArgs>} A Promise that resolves to the arguments to copy a PVC
 */
async function buildArgs(args: BuildArgsArgs): Promise<PVCCopyArgs> {
  const { parsed } = args;

  // Build args
  const result = {
    namespace: `${parsed.namespace}`,

    source: {
      deployment: {
        name: `${parsed.sourceDeploymentName}`,
      },
    },

    volume: { name: `${parsed.volumeName}` },

    dest: {
      storageClass: {
        name: `${parsed.destStorageclassName}`,
      },
    },

    copy: {
      requireNode: parsed.requireNode ? `${parsed.requireNode}` : undefined,
    },

    cleanup: !!parsed.cleanup,
  };

  // check the args are valid
  await checkPVCCopyArgs(result);

  return result;
}


interface BuildCopyJobArgs {
  ns: string;

  // Source PVC name
  srcPVCName: string;

  // Destination PVC name
  destPVCName: string;

  // ID of the build job
  id?: string;

  // Whether or not to require placement on a specific node
  requireNode?: string;

  logger?: Logger;
}

/**
 * Build job that will perform the copy
 *
 * @param {BuildJobArgs} args
 * @returns {Promise<k8s.V1Job>} A Promise that resolves to a V1Job which will do the copying
 */
async function buildCopyJob(args: BuildCopyJobArgs): Promise<k8s.V1Job> {
  const { ns, srcPVCName, destPVCName, logger } = args;
  const id = args.id ?? generateRandomString(6).toLowerCase();

  const resource: k8s.V1Job = {
    kind: "Job",
    apiVersion: "batch/v1",

    metadata: {
      name: `pvcloney-job-${id}`,
      namespace: ns,
    },

    spec: {
      template: {
        spec: {
          restartPolicy: "Never",

          containers: [
            {
              name: "copy",
              image: "alpine:3.14.2",
              imagePullPolicy: "IfNotPresent",
              command: [
                "/bin/ash",
                "-c",
                `
echo -e "[info] Installing rsync";
apk add rsync;

[ -z "$SOURCE_DIR" ] && exit "SOURCE_DIR not defined";
[ -z "$DEST_DIR" ] && exit "DEST_DIR not defined";

echo -e "[info] starting transfer from source dir [$SOURCE_DIR}] to dest dir [$DEST_DIR]";
chown $( stat -c '%a' $SOURCE_DIR ) $DEST_DIR;
chmod $( stat -c '%a' $SOURCE_DIR ) $DEST_DIR;
rsync -ravpPS --delete $SOURCE_DIR/ $DEST_DIR;
du -shxc $SOURCE_DIR $DEST_DIR;
`,
              ],

              env: [
                // Initially the container will mount the new PVC and wait
                { name: "SOURCE_DIR", value: "/data/src" },
                { name: "DEST_DIR", value: "/data/dest" },
              ],

              volumeMounts: [
                { name: "src", mountPath: "/data/src" },
                { name: "dest", mountPath: "/data/dest" },
              ],
            },
          ],

          volumes: [
            {
              name: "src",
              persistentVolumeClaim: {
                claimName: srcPVCName,
              },
            },
            {
              name: "dest",
              persistentVolumeClaim: {
                claimName: destPVCName,
              },
            },
          ],

        }
      }
    }
  };

  // Add node name if required
  if (args.requireNode && resource?.spec?.template.spec) {
    logger?.warn(`restricting node to [${args.requireNode}]`);
    resource.spec.template.spec.nodeSelector = {
      "kubernetes.io/hostname": args.requireNode,
    };
  }

  logger?.debug({ msg: "built new copy Job", resource });

  return resource;
}

interface BuildDestinationPVCArgs {
  ns: string;

  srcPVC: k8s.V1PersistentVolumeClaim;

  storageClassName: string;

  // ID of the build job
  id?: string;

  // PVC size (k8s syntax)
  size?: string;

  logger?: Logger;
}

/**
 * Build PVC that will receive the copy
 *
 * @param {BuildDestinationPVCArgs} args
 * @returns {Promise<k8s.V1PersistentvolumeClaim>} A Promise that resolves to a V1PersistentvolumeClaim which will receive the data
 */
async function buildDestinationPVC(args: BuildDestinationPVCArgs): Promise<k8s.V1PersistentVolumeClaim> {
  const { srcPVC, storageClassName, ns, logger } = args;
  const id = args.id ?? generateRandomString(6).toLowerCase();

  const resource: k8s.V1PersistentVolumeClaim = {
    kind: "PersistentVolumeClaim",
    apiVersion: "v1",

    metadata: {
      name: `pvcloney-${srcPVC.metadata?.name}-${id}`,
      namespace: ns,
    },

    spec: {
      storageClassName: storageClassName,
    },
  };

  if (resource.metadata && srcPVC.metadata?.labels) { resource.metadata.labels = srcPVC.metadata.labels; }
  if (resource.spec && srcPVC.spec?.accessModes) { resource.spec.accessModes = srcPVC.spec.accessModes; }
  if (resource.spec && srcPVC.spec?.resources) { resource.spec.resources = srcPVC.spec.resources; }

  logger?.debug({ msg: "built new PVC", resource });

  return resource;
}

async function run(this: Command): Promise<void> {
  const logger = getDefaultLogger();

  // Build arguments to perform copy
  const copyArgs = await buildArgs({
    parsed: this.opts(),
    env: process.env,
    argv: process.argv,
  });

  // Ensure the copy args are valid
  await checkPVCCopyArgs(copyArgs);

  // Connect to k8s, build client
  logger.info("connecting to k8s...");
  const kubeconfig = await connectToK8s();
  const k8sCoreV1API = kubeconfig.makeApiClient(k8s.CoreV1Api);
  const k8sAppsV1API = kubeconfig.makeApiClient(k8s.AppsV1Api);
  const k8sBatchV1API = kubeconfig.makeApiClient(k8s.BatchV1Api);

  let resp;

  let srcDeployment: k8s.V1Deployment;
  let srcDeploymentOriginalReplicaCount = 1;
  logger.debug({ msg: "args", args: copyArgs});
  const srcDeploymentName = copyArgs.source.deployment.name;
  const ns = copyArgs.namespace;

  const volumeName = copyArgs.volume.name;
  let volumeSpec;

  let srcPVCName: string;
  let srcPVC: k8s.V1PersistentVolumeClaim;

  const operationId = generateRandomString(6).toLowerCase();

  ///////////////////////////////
  // Step: Find the deployment //
  ///////////////////////////////

  // Find the deployment
  try {
    resp = await k8sAppsV1API.readNamespacedDeployment(srcDeploymentName, ns);
    if (!resp || !resp.body) { throw new Error("Missing/Invalid response from k8s API call"); }

    srcDeployment = resp.body;
    if (!srcDeployment) { throw new Error("failed to find determine source PVC name"); }

    srcDeploymentOriginalReplicaCount = srcDeployment.spec?.replicas ?? 1;

    volumeSpec = (resp.body.spec?.template?.spec?.volumes ?? []).find(v => v.name === volumeName);
    if (!volumeSpec) { throw new Error(`failed to find volume spec with name [${volumeName}]`); }

    if (!volumeSpec.persistentVolumeClaim) {
      throw new Error(`Volume with name [${volumeName}] is not of type PersistentVolumeClaim`);
    }
  } catch (err: any) {
    logger?.error({
      msg: `failed to valid deployment named [${srcDeploymentName}]`,
      errMsg: err?.toString(),
      k8sResponse: err?.response,
    });

    throw new Error("Failed to find valid deployment");
  }

  srcPVCName = volumeSpec.persistentVolumeClaim.claimName;
  if (!srcPVCName) { throw new Error("failed to find determine source PVC name"); }

  if (typeof srcDeploymentOriginalReplicaCount === "undefined") { throw new Error("Failed to retrieve source deployment replicas"); }

  ////////////////////////////////
  // Step: Find the related PVC //
  ////////////////////////////////

  // Find the related PVC
  try {
    resp = await k8sCoreV1API.readNamespacedPersistentVolumeClaim(srcPVCName, ns);
    if (!resp || !resp.body) { throw new Error("Missing/Invalid response from k8s API call"); }

    srcPVC = resp.body;
  } catch (err: any) {
    logger?.error({
      msg: `failed to expected PVC [${srcPVCName}]\nERROR: ${err.toString()}`,
      errMsg: err?.toString(),
      k8sResponse: err?.response,
    });

    throw new Error(`Failed to expected PVC [${srcPVCName}]`);
  }

  if (!srcPVC) { throw new Error(`failed to retrieve source PVC w/ name [${srcPVCName}]`); }
  logger?.info(`successfully found PVC [${srcPVCName}]`);

  ////////////////////////////////////////////////////////
  // Step: Ensure the OLD PV reclaim policy is 'Retain' //
  ////////////////////////////////////////////////////////

  if (!srcPVC.spec?.volumeName) { throw new Error("volume name is missing from src PVC"); }
  const srcPVName: string = srcPVC.spec?.volumeName;

  // Ensure the PV underlying the new PVC is set to be retained after job completion
  try {
    const patch = [
      {
        op: "replace",
        path: "/spec/persistentVolumeReclaimPolicy",
        value: "Retain",
      },
    ];

    // patch the deployment
    resp = await k8sCoreV1API.patchPersistentVolume(
      srcPVName,
      patch,
      undefined,
      undefined,
      undefined,
      undefined,
      { "headers": { "Content-Type": k8s.PatchUtils.PATCH_FORMAT_JSON_PATCH }},
    );
    if (!resp || !resp.body) { throw new Error("Missing/Invalid response from k8s API call"); }
    logger.info(`patched PV [${srcPVName}] reclaim policy to 'Retain'`);

    // Wait until the status reads reclaim on the PV
    await waitFor(
      async () => {
        try {
          const resp = await k8sCoreV1API.readPersistentVolumeStatus(srcPVName, ns);
          if (resp.body && resp.body.spec?.persistentVolumeReclaimPolicy === "Retain") { return true; }
        } catch (err: any) {
          logger?.warn({
            msg: `error while reading status of PV [${srcPVName}]`,
            errMsg: err?.toString(),
            k8sResponse: err?.response,
          });
        }
        return null;
      },

      { timeoutMs: DEFAULT_TIMEOUT_MS },
    );
    logger.info(`PV [${srcPVName}] successfully set to 'Retain'`);

  } catch (err: any) {
    logger?.error({
      msg: `failed to set PV [${srcPVName}] reclaim policy to 'Retain'\nERROR: ${err.toString()}`,
      errMsg: err?.toString(),
      k8sResponse: err?.response,
    });

    throw new Error(`Failed to set PV [${srcPVName}] reclaim policy to 'Retain'`);
  }

  //////////////////////////////////
  // Step: Create destination PVC //
  //////////////////////////////////

  // Create the destination PVC resource
  let destPVC: k8s.V1PersistentVolumeClaim;
  const destPVCResource = await buildDestinationPVC({
    ns,
    storageClassName: copyArgs.dest.storageClass.name,
    id: operationId,
    srcPVC,
  });
  const destPVCName = destPVCResource.metadata?.name;
  if (!destPVCName) { throw new Error("Failed to build PVC resource"); }

  // Apply the resource to the cluster
  try {
    resp = await k8sCoreV1API.createNamespacedPersistentVolumeClaim(ns, destPVCResource);
    if (!resp || !resp.body) { throw new Error("Missing/Invalid response from k8s API call"); }

  } catch (err: any) {
    logger?.error({
      msg: `failed to create PVC [${destPVCName}]\nERROR: ${err.toString()}`,
      errMsg: err?.toString(),
      k8sResponse: err?.response,
    });

    throw new Error(`Failed to create PVC [${destPVCName}]`);
  }

  // Wait for PVC status to be bound
  try {
    logger?.info(`waiting for destination PVC [${destPVCName}] to be bound...`);
    await waitFor(
      async () => {
        try {
          const resp = await k8sCoreV1API.readNamespacedPersistentVolumeClaimStatus(destPVCName, ns);
          if (resp.body && resp.body.status?.phase?.toLowerCase() === "bound") { return true;  }
        } catch (err: any) {
          logger?.warn({
            msg: `error while reading status for destination PVC [${destPVCName}]`,
            errMsg: err?.toString(),
            k8sResponse: err?.response,
          });
        }
        return null;
      },

      { timeoutMs: DEFAULT_TIMEOUT_MS },
    );
    logger.info(`PVC [${destPVCName}] successfully bound...`);
  } catch (err: any) {
    logger?.error(`failed to get destination PVC [${destPVCName}] status\nERROR: ${err.toString()}`);
    throw err;
  }

  // Re-Retrieve the PVC
  try {
    resp = await k8sCoreV1API.readNamespacedPersistentVolumeClaim(destPVCName, ns);
    if (!resp || !resp.body) { throw new Error("Missing/Invalid response from k8s API call"); }

    destPVC = resp.body;
  } catch (err: any) {
    logger?.error({
      msg: `failed to create PVC [${destPVCName}]\nERROR: ${err.toString()}`,
      errMsg: err?.toString(),
      k8sResponse: err?.response,
    });

    throw new Error(`Failed to create PVC [${destPVCName}]`);
  }

  if (!destPVC) { throw new Error(`Failed to create PVC [${destPVCName}]`); }
  logger?.info(`successfully created destination PVC [${destPVCName}]`);


  //////////////////////////////////////
  // Step: Create job to perform copy //
  //////////////////////////////////////

  // Create the job that will do the copying
  let job: k8s.V1Job;
  const jobResource = await buildCopyJob({
    ns,
    id: operationId,
    srcPVCName,
    destPVCName,
    requireNode: copyArgs.copy.requireNode,
  });
  const jobName = jobResource.metadata?.name;
  if (!jobName) { throw new Error("Failed to build Job resource"); }

  try {
    resp = await k8sBatchV1API.createNamespacedJob(ns, jobResource);
    if (!resp || !resp.body) { throw new Error("Missing/Invalid response from k8s API call"); }

    job = resp.body;

  } catch (err: any) {
    logger?.error({
      msg: `failed to create Job [${jobName}]\nERROR: ${err.toString()}`,
      errMsg: err?.toString(),
      k8sResponse: err?.response,
    });

    throw new Error(`Failed to create job [${jobName}]`);
  }

  if (!job) { throw new Error(`Failed to create job [${jobName}]`); }
  logger?.info(`successfully created copy job [${jobName}]`);

  //////////////////////////////////////
  // Step: Wait for job to be running //
  //////////////////////////////////////

  try {
    logger?.info(`waiting for Job [${jobName}] to become active...`);
    await waitFor(
      async () => {
        try {
          const resp = await k8sBatchV1API.readNamespacedJobStatus(jobName, ns);
          if (resp.body && typeof resp.body.status?.active === "number" && resp.body.status?.active >= 0) {
            return true;
          }
        } catch (err: any) {
          logger?.warn({
            msg: `error while reading status for Job [${jobName}]`,
            errMsg: err?.toString(),
            k8sResponse: err?.response,
          });
        }
        return null;
      },

      { timeoutMs: DEFAULT_TIMEOUT_MS },
    );
  } catch (err: any) {
    logger?.error({
      msg: `failed to get job [${jobName}] status\nERROR: ${err.toString()}`,
      errMsg: err?.toString(),
      k8sResponse: err?.response,
    });

    throw new Error(`Failed to get job [${jobName}] status`);

  }

  logger?.info(`Job [${jobName}] has become active`);

  ////////////////////////////////////////////////////////
  // Step: Ensure the new PV reclaim policy is 'Retain' //
  ////////////////////////////////////////////////////////

  // Re-Retrieve the dest PVC (in-use by the job at present
  try {
    resp = await k8sCoreV1API.readNamespacedPersistentVolumeClaim(destPVCName, ns);
    if (!resp || !resp.body) { throw new Error("Missing/Invalid response from k8s API call"); }

    destPVC = resp.body;
  } catch (err: any) {
    logger?.error({
      msg: `failed to create PVC [${destPVCName}]\nERROR: ${err.toString()}`,
      errMsg: err?.toString(),
      k8sResponse: err?.response,
    });

    throw new Error(`Failed to create PVC [${destPVCName}]`);
  }

  // Ensure dest PVC has a PV attached, extract it's name
  if (!destPVC.spec?.volumeName) {
    logger?.warn({ msg: "volume name missing from dest PVC", data: { destPVC }});
    throw new Error("volume name is missing from destination PVC");
  }
  const destPVName = destPVC.spec?.volumeName;

  // Ensure the PV underlying the new PVC is set to be retained after job completion
  try {
    const patch = [
      {
        op: "replace",
        path: "/spec/persistentVolumeReclaimPolicy",
        value: "Retain",
      },
    ];

    // patch the deployment
    resp = await k8sCoreV1API.patchPersistentVolume(
      destPVName,
      patch,
      undefined,
      undefined,
      undefined,
      undefined,
      { "headers": { "Content-Type": k8s.PatchUtils.PATCH_FORMAT_JSON_PATCH }},
    );
    if (!resp || !resp.body) { throw new Error("Missing/Invalid response from k8s API call"); }
    logger.info(`patched PV [${destPVName}] reclaim policy to 'Retain'`);

    // Wait until the status reads reclaim on the PV
    await waitFor(
      async () => {
        try {
          const resp = await k8sCoreV1API.readPersistentVolumeStatus(destPVName, ns);
          if (resp.body && resp.body.spec?.persistentVolumeReclaimPolicy === "Retain") { return true; }
        } catch (err: any) {
          logger?.warn({
            msg: `error while reading status of PV [${destPVName}]`,
            errMsg: err?.toString(),
            k8sResponse: err?.response,
          });
        }
        return null;
      },

      { timeoutMs: DEFAULT_TIMEOUT_MS },
    );
    logger.info(`PV [${destPVName}] successfully set to 'Retain'`);

  } catch (err: any) {
    logger?.error({
      msg: `failed to set PV [${destPVName}] reclaim policy to 'Retain'\nERROR: ${err.toString()}`,
      errMsg: err?.toString(),
      k8sResponse: err?.response,
    });

    throw new Error(`Failed to set PV [${destPVName}] reclaim policy to 'Retain'`);
  }

  /////////////////////////////////
  // Step: Scale deployment to 0 //
  /////////////////////////////////

  // Scale the deployment to 0
  // (this should enable the PVC to be bound to the job)
  try {

    const patch = [
      {
        op: "replace",
        path: "/spec/replicas",
        value: 0,
      },
    ];

    // patch the deployment
    resp = await k8sAppsV1API.patchNamespacedDeployment(
      srcDeploymentName,
      ns,
      patch,
      undefined,
      undefined,
      undefined,
      undefined,
      { "headers": { "Content-Type": k8s.PatchUtils.PATCH_FORMAT_JSON_PATCH }},
    );
    if (!resp || !resp.body) { throw new Error("Missing/Invalid response from k8s API call"); }
    logger.info(`patched deployment [${srcDeploymentName}] scale to 0`);

    // Wait until the scale to reach 0
    await waitFor(
      async () => {
        try {
          const resp = await k8sAppsV1API.readNamespacedDeploymentScale(srcDeploymentName, ns);
          if (resp.body && resp.body.status && resp.body.status.replicas === 0) { return true; }
        } catch (err: any) {
          logger?.warn({
            msg: `error while reading deployment scale for deployment [${srcDeploymentName}]`,
            errMsg: err?.toString(),
            k8sResponse: err?.response,
          });
        }
        return null;
      },

      { timeoutMs: REPLICA_SCALE_TIMEOUT_MS },
    );

  } catch (err: any) {
    logger?.error({
      msg: `failed to scale deployment [${srcDeploymentName}] to 0\nERROR: ${err.toString()}`,
      errMsg: err?.toString(),
      k8sResponse: err?.response,
    });

    throw new Error(`Failed to scale deployment [${srcDeploymentName}] to 0`);
  }

  logger?.info(`Successfully scaled deployment [${srcDeploymentName}] to 0`);

  //////////////////////////////////////
  // Step: Waiting for job completion //
  //////////////////////////////////////

  // Wait until job completion (with timeout) https://www.npmjs.com/package/async-wait-for-promise
  try {
    // Wait for the job to be running (it should be stopped)
    logger.info(`waiting for job [${jobName}] to complete...`);
    await waitFor(
      async () => {
        try {
          const resp = await k8sBatchV1API.readNamespacedJobStatus(jobName, ns);
          if (resp.body && resp.body.status?.completionTime !== undefined) { return true; }
        } catch (err: any) {
          logger?.warn({
            msg: `error while reading job status for job [${jobName}]`,
            errMsg: err?.toString(),
            k8sResponse: err?.response,
          });
        }
          return null;
      },

      { timeoutMs: COPY_JOB_TIMEOUT_MS },
    );
  } catch (err: any) {
    logger?.error({
      msg: `failed to update & complete Job [${jobName}]\nERROR: ${err.toString()}`,
      errMsg: err?.toString(),
      k8sResponse: err?.response,
    });

    throw new Error(`faile to update & complete Job [${jobName}]`);
  }
  logger.info(`job [${jobName}] has completed...`);

  logger?.info("successfully copied PVC contents");

  /////////////////////////////
  // Step: Delete source PVC //
  /////////////////////////////

  // NOTE: the PVC is gone, but the PV will remain!

  try {
    // Delete the PVC
    logger.info(`deleting [${srcPVCName}] (original)...`);
    resp = await k8sCoreV1API.deleteNamespacedPersistentVolumeClaim(srcPVCName, ns);
    if (!resp || !resp.body) { throw new Error("Missing/Invalid response from k8s API call"); }
    logger.info(`Successfully deleted [${srcPVCName}] (original)...`);

    // Generate patch to remove finalizer
    const patch = [
      { op: "remove", path: "/metadata/finalizers" },
    ];

    // Remove finalizer on the source PVC
    logger.info(`removing finalizers for PVC [${srcPVCName}]`);
    resp = await k8sCoreV1API.patchNamespacedPersistentVolumeClaim(
      srcPVCName,
      ns,
      patch,
      undefined,
      undefined,
      undefined,
      undefined,
      { "headers": { "Content-Type": k8s.PatchUtils.PATCH_FORMAT_JSON_PATCH }},
    );
    if (!resp || !resp.body) { throw new Error("Missing/Invalid response from k8s API call"); }

    // Wait for the PVC delete to complete
    logger.info(`Waiting for [${srcPVCName}] to be fully terminated...`);
    await waitFor(
      async () => {
        try {
          resp = await k8sCoreV1API.readNamespacedPersistentVolumeClaimStatus(srcPVCName, ns);
        } catch (err: any) {
          // Stop polling once the PVC is deleted
          if (err?.response?.statusCode === 404) { return true; }
        }

        return null;
      },

      { timeoutMs: ORIGINAL_PVC_DELETE_TIMEOUT_MS },
    );
    logger.info(`PVC [${srcPVCName}] (original) successfully deleted...`);

  } catch (err: any) {
    logger?.error({
      msg: `failed to delete PVC [${srcPVCName}]\nERROR: ${err.toString()}`,
      errMsg: err?.toString(),
      k8sResponse: err?.response,
    });

    throw new Error(`Failed to delete PVC [${srcPVCName}]`);
  }

  logger?.info(`successfully deleted PVC [${srcPVCName}] (the PV [${srcPVName}] remains)`);

  ////////////////////////////////////////////////////////
  // Step: Create new dest PVC with same name as source //
  ////////////////////////////////////////////////////////

  // Build a destination PVC again
  const newSourcePVCResource = await buildDestinationPVC({
    ns,
    storageClassName: copyArgs.dest.storageClass.name,
    id: operationId,
    srcPVC,
  });

  // Modify the PVC to have the original name as the source PVC,
  // but the spec.volumeName of the *cloned* destination PV
  if (typeof newSourcePVCResource.metadata !== 'object') { throw new Error("unexpectedly invalid PVC resource generated"); }
  newSourcePVCResource.metadata.name = srcPVCName;
  if (typeof newSourcePVCResource.spec !== 'object') { throw new Error("unexpectedly invalid PVC resource generated"); }
  newSourcePVCResource.spec.volumeName = destPVName;

  // Create the new PVC with original pre-copy PVC name
  try {
    resp = await k8sCoreV1API.createNamespacedPersistentVolumeClaim(ns, newSourcePVCResource);
    if (!resp || !resp.body) { throw new Error("Missing/Invalid response from k8s API call"); }

  } catch (err: any) {
    logger?.error({
      msg: `failed to create (new) PVC [${srcPVCName}]\nERROR: ${err.toString()}`,
      errMsg: err?.toString(),
      k8sResponse: err?.response,
    });

    throw new Error(`Failed to create (new) PVC [${srcPVCName}]`);
  }

  // Retrieve the new source PVC
  let newSourcePVC;
  try {
    resp = await k8sCoreV1API.readNamespacedPersistentVolumeClaim(srcPVCName, ns);
    if (!resp || !resp.body) { throw new Error("Missing/Invalid response from k8s API call"); }

    newSourcePVC = resp.body;
  } catch (err: any) {
    logger?.error({
      msg: `failed to create PVC [${srcPVCName}]\nERROR: ${err.toString()}`,
      errMsg: err?.toString(),
      k8sResponse: err?.response,
    });

    throw new Error(`Failed to create PVC [${srcPVCName}]`);
  }

  if (!newSourcePVC) { throw new Error(`Failed to create PVC [${srcPVCName}]`); }
  logger?.info(`successfully created destination PVC [${srcPVCName}]`);

  /////////////////////////////////////////////////////
  // Step: Set claimRef on dest PV to new source PVC //
  /////////////////////////////////////////////////////

  // NOTE: After this, the dest PVC will be in "Lost" status

  // Determine the dest volume name
  // Set the volume name of the original PVC to the PV for the destination (copied) PVC
  try {
    const patch = [
      {
        op: "replace",
        path: "/spec/claimRef",
        value: {
          apiVersion: newSourcePVC.apiVersion,
          kind: newSourcePVC.kind,
          name: newSourcePVC.metadata?.name,
          namespace: newSourcePVC.metadata?.namespace,
          resourceVersion: newSourcePVC.metadata?.resourceVersion,
          uid: newSourcePVC.metadata?.uid,
        },
      },
    ];

    // Patch the source PVC
    logger.info(`setting PVC [${destPVName}] claimRef to [${srcPVCName}]`);
    resp = await k8sCoreV1API.patchPersistentVolume(
      destPVName,
      patch,
      undefined,
      undefined,
      undefined,
      undefined,
      { "headers": { "Content-Type": k8s.PatchUtils.PATCH_FORMAT_JSON_PATCH }},
    );
    if (!resp || !resp.body) { throw new Error("Missing/Invalid response from k8s API call"); }

    /////////////////////////////////////////////////////////
    // Step: Wait until the sourcePVC (recreated) is bound //
    /////////////////////////////////////////////////////////

    await waitFor(
      async () => {
        try {
          const resp = await k8sCoreV1API.readNamespacedPersistentVolumeClaim(srcPVCName, ns);
          // At this point, the PVC should be bound *and* in the new storage class
          const bound = resp.body && resp.body.status?.phase === 'Bound';
          const rightStorageClass = resp.body.spec?.storageClassName === copyArgs.dest.storageClass.name;
          if (bound && rightStorageClass) { return true; }
        } catch (err: any) {
          logger?.warn({
            msg: `error while reading PVC [${srcPVName}]`,
            errMsg: err?.toString(),
            k8sResponse: err?.response,
          });
        }
        return null;
      },

      { timeoutMs: REPLICA_SCALE_TIMEOUT_MS },
    );

  } catch (err: any) {
    logger?.error({
      msg: `failed to update claimRef of PV [${destPVName}]\nERROR: ${err.toString()}`,
      errMsg: err?.toString(),
      k8sResponse: err?.response,
    });

    throw new Error(`Failed to update claimRef of PV [${destPVName}]`);
  }

  logger.info(`Successfully updated PV [${destPVName}] claimRef to [${srcPVCName}]`);
  logger.info(`PVC [${destPVCName}] status is now 'Lost'`);
  logger.info(`PVC [${srcPVCName}] status is now 'Bound' and is on the new storage class [${copyArgs.dest.storageClass.name}]`);

  // Wait for PVC status to change from pending
  try {
    logger?.info(`waiting for destination PVC [${srcPVCName}] to be bound...`);
    await waitFor(
      async () => {
        try {
          const resp = await k8sCoreV1API.readNamespacedPersistentVolumeClaimStatus(srcPVCName, ns);
          if (resp.body && resp.body.status?.phase?.toLowerCase() === "bound") { return true;  }
        } catch (err: any) {
          logger?.warn({
            msg: `error while reading status for destination PVC [${srcPVCName}]`,
            errMsg: err?.toString(),
            k8sResponse: err?.response,
          });
        }
        return null;
      },

      { timeoutMs: DEFAULT_TIMEOUT_MS },
    );
    logger.info(`PVC [${srcPVCName}] successfully bound...`);
  } catch (err: any) {
    logger?.error(`failed to get destination PVC [${srcPVCName}] status\nERROR: ${err.toString()}`);
    throw err;
  }

  ///////////////////////////////////////////////////////////
  // Step: Scale deployment back to original replica count //
  ///////////////////////////////////////////////////////////

  // Scale the deployment back to original
  // (this should enable the PVC to be bound to the job)
  try {

    const patch = [
      {
        op: "replace",
        path: "/spec/replicas",
        value: srcDeploymentOriginalReplicaCount,
      },
    ];

    // patch the deployment
    resp = await k8sAppsV1API.patchNamespacedDeployment(
      srcDeploymentName,
      ns,
      patch,
      undefined,
      undefined,
      undefined,
      undefined,
      { "headers": { "Content-Type": k8s.PatchUtils.PATCH_FORMAT_JSON_PATCH }},
    );
    if (!resp || !resp.body) { throw new Error("Missing/Invalid response from k8s API call"); }
    logger.info(`patched deployment [${srcDeploymentName}] scale to 0`);

    // Wait until the scale to reach original count
    await waitFor(
      async () => {
        try {
          const resp = await k8sAppsV1API.readNamespacedDeploymentScale(srcDeploymentName, ns);
          if (resp.body && resp.body.status && resp.body.status.replicas === srcDeploymentOriginalReplicaCount) { return true; }
        } catch (err: any) {
          logger?.warn({
            msg: `error while reading deployment scale for deployment [${srcDeploymentName}]`,
            errMsg: err?.toString(),
            k8sResponse: err?.response,
          });
        }
        return null;
      },

      { timeoutMs: REPLICA_SCALE_TIMEOUT_MS },
    );

  } catch (err: any) {
    logger?.error({
      msg: `failed to scale deployment [${srcDeploymentName}] back to [${srcDeploymentOriginalReplicaCount}]\nERROR: ${err.toString()}`,
      errMsg: err?.toString(),
      k8sResponse: err?.response,
    });

    throw new Error(`Failed to scale deployment [${srcDeploymentName}] to [${srcDeploymentOriginalReplicaCount}]`);
  }

  logger.warn(`Source PV [${srcPVName}] must be deleted manually!`);
  logger.info("PVC clone is complete");

  /////////////
  // Cleanup //
  /////////////

  if (!copyArgs.cleanup) { return; }

  // Clean up
  // Delete created job
  try {
    logger?.info(`cleaning up created job [${jobName}]`);
    resp = await k8sBatchV1API.deleteNamespacedJob(jobName, ns);
    if (!resp || !resp.body) { throw new Error("Missing/Invalid response from k8s API call"); }
  } catch (err: any) {
    logger?.error({
      msg: `failed to delete Job [${jobName}]\nERROR: ${err.toString()}`,
      errMsg: err?.toString(),
      k8sResponse: err?.response,
    });

    throw new Error(`Failed to delete job [${jobName}]`);
  }

  // Delete created destination PVC
  try {
    logger?.info(`cleaning up created destination PVC [${destPVCName}]`);
    resp = await k8sCoreV1API.deleteNamespacedPersistentVolumeClaim(destPVCName, ns);
    if (!resp || !resp.body) { throw new Error("Missing/Invalid response from k8s API call"); }

    // Generate patch to remove finalizer
    const patch = [
      { op: "remove", path: "/metadata/finalizers" },
    ];

    // Remove finalizer on the source PVC
    logger.info(`removing finalizers for PVC [${destPVCName}]`);
    resp = await k8sCoreV1API.patchNamespacedPersistentVolumeClaim(
      destPVCName,
      ns,
      patch,
      undefined,
      undefined,
      undefined,
      undefined,
      { "headers": { "Content-Type": k8s.PatchUtils.PATCH_FORMAT_JSON_PATCH }},
    );
    if (!resp || !resp.body) { throw new Error("Missing/Invalid response from k8s API call"); }

  } catch (err: any) {
    logger?.error({
      msg: `failed to delete dest PVC [${destPVCName}]\nERROR: ${err.toString()}`,
      errMsg: err?.toString(),
      k8sResponse: err?.response,
    });

    throw new Error(`Failed to patch dest PVC [${destPVCName}]`);
  }

}

async function main(): Promise<void> {
  // Command program definition
  program
    .version(VERSION)
    .description("Clone and Swap a PVC")
    .requiredOption(
      "--namespace <namespace>",
      "The namespace where everything happens",
    )
    .requiredOption(
      "--source-deployment-name <deployment>",
      "The deployment which will have it's volume swapped",
    )
    .requiredOption(
      "--dest-storageclass-name <storage class>",
      "The storage class for the new PVC that will be created",
    )
    .requiredOption(
      "--volume-name <volume>",
      "The name of the volume to be swapped, must be a PVC",
    )
    .option(
      "--require-node <node name>",
      "Force the copy workload to be on the specified node",
    )
    .option(
      "--cleanup",
      "whether to perform cleanup of automatically created resources (default: true)",
      true,
    )
    .action(run);

  await program.parseAsync(process.argv);
}


// https://stackoverflow.com/questions/45136831/node-js-require-main-module
if (process.argv[1] === fileURLToPath(import.meta.url)) { main(); }
