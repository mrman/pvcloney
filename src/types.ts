export interface PVCCopyArgs {
  namespace: string;

  // Deployment which will be
  source: {
    deployment: {
      // The name of the existing deployment workload
      name: string;
    };
  };

  volume: {
    // The name of the existing volume on the deployment we will be cloning
    name: string;
  };

  dest: {
    storageClass: {
      // The name of the storage class from which to create the new PVC
      name: string;
    },
  };

  copy: {
    // Whether the copy deployment job should be forced to occur on a given node
    requireNode?: string;
  }

  cleanup?: boolean;
}

export async function checkPVCCopyArgs(args: PVCCopyArgs): Promise<void> {
  if (!args?.source?.deployment?.name) { throw new Error("Missing deployment name, please specify it with --deployment-name"); }
  if (!args?.volume?.name) { throw new Error("Missing PVC name, please specify it with --pvc-name"); }
}
