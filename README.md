# `pvcloney`

**Also consider using [`pv-migrate`][pv-migrate] which is more robust!**

A painfully long and procedural script for copying the data of a PVC to a different PVC (with a likely different StorageClass). This script also helps if you have a storage provider that doesn't support CSI cloning/snapshotting in the the same storage class as well (which is increasingly rare).

This script will:

- Always mark the original PV's retention policy as `Retain` (it's up to you to ensure the application works fine after the brief downtime)
- Never delete the original PV (so you can revert the changes if necessary, see [Read in case of fire](#break-in-case-of-fire))
- Leave it up to you to delete the original PV
- Require you to be patient! There are timeouts built in, so just let it do it's thing until it crashes. If you somehow get stuck but don't crash, file an issue.

Take a look at the code! Outside of being an absolute procedural rats nest with nearly no smarts applied, it's incredibly consistent (repetitive) and chunked well.

If you see an incorrect copy-pasted comment, thanks in advance for the PR.

# Notes/Caveats

- This repository works best when going from a node-constrained `StorageClass` to a node-independent one -- it attempts to mount *both volumes in a single `Job`*.
  - If you are trying to move PVCs *between* nodes, use [`pv-migrate`][pv-migrate], and `uncordon`ing your node temporarily
- The destination (new clone) PVC must have `volumeBindingMode: Immediate` set (the script will attempt to wait for the PVC to be properly bound)
- If you'd like to *avoid* a certain node, make sure to `cordon` it before running the script (note that the original workload *could already* be running on the now-cordoned node)

# Quickstart

## With Docker

The tool can also be run as a docker image, so you can avoid downloading and building the source:

```console
$ docker run --rm \
    -v $KUBECONFIG:/k8s/config:ro \
    -e KUBECONFIG=/k8s/config \
    registry.gitlab.com/mrman/pvcloney/cli:0.2.2 \
        --namespace <namespace> \
        --source-deployment-name <workload name> \
        --volume-name <name of volume in workload> \
        --dest-storageclass-name <new storage class> \
```

Some notes about the command above:

- `<namespace>` is where the workload you'd like to change the storage out from under is running
- `<workload name>` is the name of the workload (currently only `Deployment`s are supported)
- `<name of the volume in workload>` indicates the volume name, *not* the old persistent volume name (the volume could be named anything in the `Deployment` YAML)

# As a Kubernetes Job

The tool can also be run in Kubernetes as a `Job` (which will spawn another `Job`), you'll need to do a bit more.

First a ServiceAccount:

```yaml
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: pvcloney
```

Then the necessary RBAC permissions

```yaml
---
kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: pvcloney
  namespace: <ns> # REPLACE: namespace
rules:
  - apiGroups:
      - ""
    resources:
      - persistentvolumes
      - persistentvolumes/status
    verbs:
      - get
      - create
      - patch
      - delete

---
kind: Role
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: pvcloney
  namespace: <ns> # REPLACE: namespace
rules:
  - apiGroups:
      - ""
    resources:
      - deployments
    verbs:
      - get
      - patch
      - list
      - delete

  - apiGroups:
      - ""
    resources:
      - persistentvolumeclaims
      - persistentvolumeclaims/status
      - persistentvolumes
    verbs:
      - get
      - create
      - patch
      - delete

  - apiGroups:
      - apps
    resources:
      - deployments/scale
      - deployments
    verbs:
      - get
      - patch
      - delete

  - apiGroups:
      - batch
    resources:
      - jobs
      - jobs/status
    verbs:
      - get
      - list
      - watch
      - create
      - delete

---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: pvcloney
  namespace: <ns> # REPLACE: namespace
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: pvcloney
subjects:
  - kind: ServiceAccount
    name: pvcloney
    namespace: <ns> # REPLACE: namespace

---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: pvcloney
  namespace: <ns> # REPLACE: namespace
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: pvcloney
subjects:
  - kind: ServiceAccount
    name: pvcloney
    namespace: <ns> # REPLACE: namespace
```

Then a `Job` that runs the container for this repo:

```yaml
---
apiVersion: batch/v1
kind: Job
metadata:
  name: pvcloney
spec:
  backoffLimit: 0
  template:
    spec:
      restartPolicy: Never

      automountServiceAccountToken: true
      serviceAccountName: pvcloney

      containers:
        - name: pvcloney
          image: registry.gitlab.com/mrman/pvcloney/cli:0.2.1
          imagePullPolicy: IfNotPresent
          args:
            - --namespace <namespace> # REPLACE: your namespace
            - --source-deployment-name <deployment> # REPLACE: the deployment you need to replace
            - --dest-storageclass-name <storageclass> # REPLACE: the new storageclass to clone into
            - --volume-name <volume attached to deployment> # REPLACE: the volume to replace
```

## Local Build & Run

To build:

```console
$ make
```

To run the tool with `make`:

```console
$ make run ARGS="--namespace ..."
```

A full example of an invocation with `make`:

```console
$ make run ARGS="--namespace your-ns --source-deployment-name maddy --dest-storageclass-name rook-raid1 --volume-name maddy-restore"
```

If you're running the built version, then:

```console
$ node dist/index.js --namespace ...
```

# Output

For a successful run, the output looks like the following:

```
{"level":30,"time":1651671097397,"pid":1,"hostname":"5baadb6fbd26","msg":"connecting to k8s..."}
{"level":30,"time":1651671099500,"pid":1,"hostname":"5baadb6fbd26","msg":"successfully found PVC [sftpgo-data]"}
{"level":30,"time":1651671101312,"pid":1,"hostname":"5baadb6fbd26","msg":"successfully created destination PVC [pvcloney-sftpgo-data-xzom81]"}
{"level":30,"time":1651671102273,"pid":1,"hostname":"5baadb6fbd26","msg":"patched PV [pvc-14f2caea-1381-42e1-9ca6-808436fca9b4] reclaim policy to 'Retain'"}
{"level":30,"time":1651671103652,"pid":1,"hostname":"5baadb6fbd26","msg":"PV [pvc-14f2caea-1381-42e1-9ca6-808436fca9b4] successfully set to 'Retain'"}
{"level":30,"time":1651671104560,"pid":1,"hostname":"5baadb6fbd26","msg":"patched PV [pvc-b3c5677e-36f0-4987-9fdf-d305b77d1926] reclaim policy to 'Retain'"}
{"level":30,"time":1651671105868,"pid":1,"hostname":"5baadb6fbd26","msg":"PV [pvc-b3c5677e-36f0-4987-9fdf-d305b77d1926] successfully set to 'Retain'"}
{"level":30,"time":1651671106756,"pid":1,"hostname":"5baadb6fbd26","msg":"successfully created copy job [pvcloney-job-xzom81]"}
{"level":30,"time":1651671106756,"pid":1,"hostname":"5baadb6fbd26","msg":"waiting for Job [pvcloney-job-xzom81] to become active..."}
{"level":30,"time":1651671108193,"pid":1,"hostname":"5baadb6fbd26","msg":"Job [pvcloney-job-xzom81] has become active"}
{"level":30,"time":1651671109388,"pid":1,"hostname":"5baadb6fbd26","msg":"patched deployment [sftpgo] scale to 0"}
{"level":30,"time":1651671110818,"pid":1,"hostname":"5baadb6fbd26","msg":"Successfully scaled deployment [sftpgo] to 0"}
{"level":30,"time":1651671110818,"pid":1,"hostname":"5baadb6fbd26","msg":"waiting for job [pvcloney-job-xzom81] to complete..."}
{"level":30,"time":1651671120277,"pid":1,"hostname":"5baadb6fbd26","msg":"job [pvcloney-job-xzom81] has completed..."}
{"level":30,"time":1651671120277,"pid":1,"hostname":"5baadb6fbd26","msg":"successfully copied PVC contents"}
{"level":30,"time":1651671120277,"pid":1,"hostname":"5baadb6fbd26","msg":"deleting [sftpgo-data] (original)..."}
{"level":30,"time":1651671121208,"pid":1,"hostname":"5baadb6fbd26","msg":"Successfully deleted [sftpgo-data] (original)..."}
{"level":30,"time":1651671121208,"pid":1,"hostname":"5baadb6fbd26","msg":"removing finalizers for PVC [sftpgo-data]"}
{"level":30,"time":1651671122126,"pid":1,"hostname":"5baadb6fbd26","msg":"Waiting for [sftpgo-data] to be fully terminated..."}
{"level":30,"time":1651671123480,"pid":1,"hostname":"5baadb6fbd26","msg":"PVC [sftpgo-data] (original) successfully deleted..."}
{"level":30,"time":1651671123480,"pid":1,"hostname":"5baadb6fbd26","msg":"successfully deleted PVC [sftpgo-data] (the PV [pvc-b3c5677e-36f0-4987-9fdf-d305b77d1926] remains)"}
{"level":30,"time":1651671125304,"pid":1,"hostname":"5baadb6fbd26","msg":"successfully created destination PVC [sftpgo-data]"}
{"level":30,"time":1651671125304,"pid":1,"hostname":"5baadb6fbd26","msg":"setting PVC [pvc-14f2caea-1381-42e1-9ca6-808436fca9b4] claimRef to [sftpgo-data]"}
{"level":30,"time":1651671136134,"pid":1,"hostname":"5baadb6fbd26","msg":"Successfully updated PV [pvc-14f2caea-1381-42e1-9ca6-808436fca9b4] claimRef to [sftpgo-data]"}
{"level":30,"time":1651671136134,"pid":1,"hostname":"5baadb6fbd26","msg":"PVC [pvcloney-sftpgo-data-xzom81] status is now 'Lost'"}
{"level":30,"time":1651671136134,"pid":1,"hostname":"5baadb6fbd26","msg":"PVC [sftpgo-data] status is now 'Bound' and is on the new storage class [rook-raid1]"}
{"level":30,"time":1651671137286,"pid":1,"hostname":"5baadb6fbd26","msg":"patched deployment [sftpgo] scale to 0"}
{"level":40,"time":1651671138675,"pid":1,"hostname":"5baadb6fbd26","msg":"Source PV [pvc-b3c5677e-36f0-4987-9fdf-d305b77d1926] must be deleted manually!"}
{"level":30,"time":1651671138675,"pid":1,"hostname":"5baadb6fbd26","msg":"PVC clone is complete"}
{"level":30,"time":1651671138675,"pid":1,"hostname":"5baadb6fbd26","msg":"cleaning up created job [pvcloney-job-xzom81]"}
{"level":30,"time":1651671139559,"pid":1,"hostname":"5baadb6fbd26","msg":"cleaning up created destination PVC [pvcloney-sftpgo-data-xzom81]"}
{"level":30,"time":1651671140557,"pid":1,"hostname":"5baadb6fbd26","msg":"removing finalizers for PVC [pvcloney-sftpgo-data-xzom81]"}
```

The original command run to generate that output was:

```
$ docker run --rm \
    -v /tmp/kubie-configx2JqWM.yaml:/k8s/config:ro \
    -e KUBECONFIG=/k8s/config \
    registry.gitlab.com/mrman/pvcloney/cli:0.2.1 \
    --namespace ragtimecloud-production \
    --source-deployment-name sftpgo \
    --dest-storageclass-name rook-raid1 \
    --volume-name data
```

In plain english, the command above expects to enter namespace `ragtimecloud-production`, find a deployment named `sftpgo`, find a volume named `data` underneath it, and copy the contents of whatever PVC it finds to an identical one with storage class `rook-raid1`.

So at the end (yes, I ran this live) I can see the following:

```console
$ k get pvc
NAME             STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS           AGE
sftpgo-data      Bound    pvc-14f2caea-1381-42e1-9ca6-808436fca9b4   16Gi       RWO            rook-raid1             8m4s
```

And the previous PV:

```console
$ k get pv | grep sftpgo
pvc-14f2caea-1381-42e1-9ca6-808436fca9b4   16Gi       RWO            Retain           Bound       ragtimecloud-production/sftpgo-data          rook-raid1                         9m10s
pvc-b3c5677e-36f0-4987-9fdf-d305b77d1926   16Gi       RWO            Retain           Released    ragtimecloud-production/sftpgo-data          longhorn                           195d
```

As you can see the previous PV (which are named `pvc-<id>` after the original PVC UIDs) is still intact and in `Released` state. **It's up to you to delete them**.

# Read in case of fire

Sometimes, things go wrong. If you find yourself in an unexpected state using this script there are three things to note:

- One of the first things this script does is set your source PV's `reclaimPolicy` to `Retain`
  - This means that even if we delete your PVC,
- This script will never attempt delete your source PV
  - The option to force this to happen isn't even there

This means that if anything ever goes wrong, you may do the following to reset:

- (if your source PVC is missing) Create the source PVC with `spec.volumeName` set to the original PV
  - It will be in the `Pending` state
- Delete the `spec.claimRef` from your original PV (which is never deleted)

After a while, your PVC should "find" the original PV, and you will be back to square one. Make sure to go around and clean up the new PVCs -- *be very careful with your CLI commands!*.

Generally resources created by this script are named `pvcloney-*`

Of course, before attempting any of this, you should be keeping regular backups, and take one specifically before attempting the copy.

## Volume stuck already bound to a different claim

If during your script execution you get this output:

```
{"level":30,"time":1651681737538,"pid":7,"hostname":"pvcloney-fnbr9","msg":"successfully deleted PVC [your-pvc] (the PV [pvc-cebdd5f9-0567-45b8-a916-c393384c9642] remains)"}
{"level":30,"time":1651681737567,"pid":7,"hostname":"pvcloney-fnbr9","msg":"waiting for destination PVC [your-pvc] to be bound..."}
{"level":50,"time":1651681917754,"pid":7,"hostname":"pvcloney-fnbr9","msg":"failed to get destination PVC [your-pvc] status\nERROR: TimeoutError: Timed out while waiting for condition"}
/app/node_modules/.pnpm/async-wait-for-promise@1.2.0/node_modules/async-wait-for-promise/dist/lib/index.js:62
                    reject(new TimeoutError());
                           ^

TimeoutError: Timed out while waiting for condition
    at /app/node_modules/.pnpm/async-wait-for-promise@1.2.0/node_modules/async-wait-for-promise/dist/lib/index.js:62:28
    at Generator.next (<anonymous>)
    at /app/node_modules/.pnpm/async-wait-for-promise@1.2.0/node_modules/async-wait-for-promise/dist/lib/index.js:8:71
    at new Promise (<anonymous>)
    at __awaiter (/app/node_modules/.pnpm/async-wait-for-promise@1.2.0/node_modules/async-wait-for-promise/dist/lib/index.js:4:12)
    at Timeout.<anonymous> (/app/node_modules/.pnpm/async-wait-for-promise@1.2.0/node_modules/async-wait-for-promise/dist/lib/index.js:51:48)
    at listOnTimeout (node:internal/timers:559:17)
    at processTimers (node:internal/timers:502:7)

Node.js v17.4.0
```

With information about the recreated duplicate PVC that matches this:

```console
k describe pvc your-pvc
Name:          your-pvc
Namespace:     your-namespace
StorageClass:  rook-raid1
Status:        Pending
Volume:        pvc-aa2d1d63-67a3-451a-a0d0-2a830b463ba3
Labels:        app=your-namespace
Annotations:   <none>
Finalizers:    [kubernetes.io/pvc-protection]
Capacity:      0
Access Modes:
VolumeMode:    Filesystem
Used By:       pvcloney-job-3uvisu-sq6gz
               pvcloney-job-79rqn8-c46hx
               pvcloney-job-hcsfcb-hr92p
Events:
  Type     Reason         Age                 From                         Message
  ----     ------         ----                ----                         -------
  Warning  FailedBinding  2s (x14 over 3m6s)  persistentvolume-controller  volume "pvc-aa2d1d63-67a3-451a-a0d0-2a830b463ba3" already bound to a different claim.
```

This is usually caused by old/extraneous PVCs (possibly from previous attempts to lcone) being around in the namespace when you start.

To fix this:

- Remove *all* extraneous PVCs related to `pvcloney` (don't worry, the PVs won't be removed)
  - Note that you may have to initiate a delete *then* remove finalizers as necessary.
- Edit the PV in question (in this example, `pvc-aa2d1d63-67a3-451a-a0d0-2a830b463ba3`), to remove it's `claimRef`

Momentarily, your PVC should bind to the newly `claimRef`-less PV.

## Everything succeeded but your Deployment didn't scale back up

If you find that the script executes cleanly but your `Deployment` doesn't scale back up, check the output carefully -- you may see something like this:

```
{"level":30,"time":1651682972401,"pid":8,"hostname":"pvcloney-gchn4","msg":"waiting for destination PVC [project-data-restore] to be bound..."}
{"level":30,"time":1651682972925,"pid":8,"hostname":"pvcloney-gchn4","msg":"PVC [project-data-restore] successfully bound..."}
{"level":30,"time":1651682972959,"pid":8,"hostname":"pvcloney-gchn4","msg":"patched deployment [project-backend] scale to 0"}
{"level":40,"time":1651682973481,"pid":8,"hostname":"pvcloney-gchn4","msg":"Source PV [pvc-6022131e-eb79-4a15-9fd7-61601f542c54] must be deleted manually!"}
{"level":30,"time":1651682973481,"pid":8,"hostname":"pvcloney-gchn4","msg":"PVC clone is complete"}
{"level":30,"time":1651682973481,"pid":8,"hostname":"pvcloney-gchn4","msg":"cleaning up created job [pvcloney-job-26ba92]"}
```

In this case, the replicas at the start of transition was `0` (possibly the result of a failed attempt earlier!) -- so you will have to scale the project yourself (`pvcloney` can't tell what the replica count was *two runs ago*):

```console
$ kubectl scale deploy <deployment> replicas=1
```

## Deleting all the temporary jobs

If you have some temporary jobs left over (you shouldn't):

```console
$ kubectl get job | grep 'pvcloney-job-' | cut -d ' ' -f 1 | xargs -I{} kubectl delete job {}
```

## Deleting all the temporary PVCs

If you have some temporary PVCs left over (you shouldn't):

```console
$ kubectl get pvc | grep 'pvcloney-' | cut -d ' ' -f 1 | xargs -I{} kubectl delete pvc {}
```

You probably will have some leftover *pods* though -- a result of the `Job`s completing successfully.

# Development

## Dependencies / Requirements

- [`direnv`][direnv] (manages your ENV)
- [`nvm`][nvm] (manages your node versions)
- [`pnpm`][pnpm] (manages your node packages)
- A kubernetes cluster you can connect to
- (optional) [`kubie`][kubie] or properly configured cluster context

## Quickstart

If you're looking to get started with development and want to simply build the project:

```console
$ make setup build
```

To get a build loop going:

```console
make build-watch
```

To run the script, see the earlier instructions for `make run`.

## Publishing

If you want to publish the repo you'll first need to make sure our ENV is set up. So in your local `.envrc`:

```
git config --local core.hooksPath $PWD/.githooks

## REPLACE THE PATH BELOW!
export DOCKER_CONFIG=/path/to/pvcloney/secrets/docker
```

[nvm]: https://nvm.io
[pnpm]: https://pnpm.io
[kubie]: https://github.com/sbstp/kubie/
[direnv]: https://direnv.net
[pv-migrate]: https://github.com/utkuozdemir/pv-migrate
